'use strict'

const express = require('express')
const productsController = require('../../controllers/products.controller')
const router = express.Router();
const authMiddleware = require('../../middlewares/auth')

router.get('/getAll', authMiddleware, productsController.index);
router.get('/getCategories', authMiddleware, productsController.getCategories);
router.post('/save', authMiddleware, productsController.save);
router.post('/update', authMiddleware, productsController.update);
router.post('/delete', authMiddleware, productsController.deleteProduct);
router.post('/findBy', authMiddleware, productsController.findby);



module.exports = router;