'use strict'

const express = require('express')
const router = express.Router();

router.use('/auth', require('./users/auth.routes'));
router.use('/products', require('./products/product.routes'));

module.exports = router;