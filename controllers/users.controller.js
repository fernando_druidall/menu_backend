'use strict'

const User = require('../models/user.model');
const service = require('../services');
const bcrypt = require('bcrypt');
const uuid = require('uuid');

function signUp(req, res) {
    const user = new User({
        uuid: uuid.v4(),
        email: req.body.email,
        displayName: req.body.displayName,
        password: req.body.password
    })

    user.save((err) => {
        if (err) {
            res.status(500).send({ message: `Error al crear el usuario: ${err}` })
        } else {
            return res.status(200).send({ token: service.createToken(user) })
        }
    })
}

// TODO falta  terminar el login, cuando hay un usuario que no existe no funciona, solo en passwords
function signIn(req, res) {
    const user = new User({
        email: req.body.email,
        password: req.body.password
    })

    User.findOne({ email: user.email }, async function (err, userFound) {
        let match = await bcrypt.compare(user.password, userFound.password);

        if (err) {
            res.status(401).send({
                message: "Nombre de usuario o password incorrectos",
            })
        }
        if (match) {
            res.status(200).send({
                message: "Bienvenido",
                token: service.createToken(userFound)
            })
        } else {
            res.status(401).send({
                message: "Nombre de usuario o password incorrectos",
            })
        }
    });
}

module.exports = {
    signUp,
    signIn
}