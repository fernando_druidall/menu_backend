'use strict'

const Product = require('../models/product.model');
const service = require('../services');
const uuid = require('uuid');

async function index(req, res) {
    let org = await service.getOrganization(req);
    Product.find({ organization: org }, function (err, products) {
        var productMap = {};

        let count = 0;
        products.forEach(function (product) {
            let provisionalObject = {
                _id: product._id,
                title: product.title,
                description: product.description,
                price: product.price
            }
            productMap[count] = provisionalObject;
            count++;
        });
        res.send(productMap);
    });
}

async function getCategories(req, res) {
    let org = await service.getOrganization(req);
    Product.find({ organization: org }, {'category' : 1, '_id' : 0}, function (err, products) {

        res.send(products);
    });
}

// TODO crear una funcion para trampar el token automatico sin tener que mandar llamar esto en cada funcion
async function findby(req, res) {
    let org = await service.getOrganization(req);

    Product.find({ category: req.body.category, organization: org }, function (err, products) {
        var productMap = {};

        let count = 0;
        products.forEach(function (product) {
            let provisionalObject = {
                product_uuid: product.product_uuid,
                title: product.title,
                description: product.description,
                price: product.price,
                category: product.category
            }
            productMap[count] = provisionalObject;
            count++;
        });
        res.send(productMap);
    });
}

async function save(req, res) {
    let org = await service.getOrganization(req);

    const product = new Product({
        product_uuid: uuid.v4(),
        organization: org,
        title: req.body.title,
        price: req.body.price,
        description: req.body.description,
        category: req.body.category
    })

    product.save((err, product) => {
        if (err) {
            res.status(500).send({ message: `No se pudo guardar el producto: ${err}` })
        } else {
            return res.status(200).send({ message: `Producto agregado correctamente: ${product}` })
        }
    })
}

async function update(req, res) {
    let org = await service.getOrganization(req);

    const query = {
        product_uuid: req.body.product_uuid,
        organization: org
    }
    Product.findOneAndUpdate(query, req.body, { upsert: false, new: true }, (err, product) => {
        if (err) {
            console.log("Ocurrio un error al intentar editar el producto: " + err);
        }

        return res.status(200).send({ message: `Producto editado correctamente: ${product}` })
    });
}

async function deleteProduct(req, res) {
    let org = await service.getOrganization(req);

    const query = {
        product_uuid: req.body.product_uuid,
        organization: org
    }
    Product.deleteOne(query, (err) => {
        if (err) {
            console.log("Ocurrio un error al intentar eliminar el producto: " + err);
        }

        return res.status(200).send({ message: `Producto eliminado correctamente` })
    });
}

module.exports = {
    index,
    save,
    update,
    deleteProduct,
    findby,
    getCategories
}