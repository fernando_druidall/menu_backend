'use strict'

const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const bcrypt = require('bcrypt-nodejs');

const ProductSchema = new Schema({
    product_uuid: { type: String, required: false },
    organization: { type: String, required: false },
    title: { type: String, lowercase: true },
    description: { type: String, lowercase: true },
    category: { type: String },
    price: { type: Number }
})

module.exports = mongoose.model('Product', ProductSchema)