const mongoose = require('mongoose');
const dbPort = 27017;
const serverPort = 3000;
const database = "restaurantes"
// TODO
// Cambiar por db correcta

process.on('uncaughtException', (error) => {
    console.log(error);
})

process.on('unhandledRejection', (error, promise) => {
    console.log('Unhandled promise rejection: ', promise);
    console.log('Error: ', error);
});

var app = require('./app')

mongoose.connect(`mongodb://localhost:${dbPort}/${database}`, { useNewUrlParser: true, useUnifiedTopology: true, useFindAndModify: false, useCreateIndex: true }, () => {
    console.log(`Database corriendo en el puerto: ${dbPort}`);

    app.listen(serverPort, () => {
        console.log(`Servidor corriento en el puerto: ${serverPort}`);
    });
});